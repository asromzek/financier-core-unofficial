/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Authentication                                                     ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier authentication libary.                                ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import axios from 'axios';
import moment from 'moment/moment';

// ##################################################
//  Auth function definition.
// ##################################################

export function Auth () {
  //
  // ==================================================
  //  Local variables.
  // ==================================================

  let _authenticated = false;
  let _email = null;
  let _userdb = '';
  let _isValidSub = false;

  const $loginStatus = new BehaviorSubject(false);
  const $loginSuccess = new Subject();
  const $authChanged = new BehaviorSubject(false);
  const $offlineChanged = new BehaviorSubject(false);
  const $isValidSub = new BehaviorSubject(true);

  const host = window.location.origin + '/db/_session';
  const config = {
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json'
    }
  };

  // ==================================================
  //  Update authentication information.
  // ==================================================

  function updateAuth (auth, email, db, valid, message) {
    _authenticated = auth;
    _email = email;
    _userdb = db;
    _isValidSub = valid;

    $authChanged.next(auth);
    $offlineChanged.next(!auth);
    $isValidSub.next(valid);
    console.log(message);

    if (_email) {
      console.log('Email: ' + _email);
      console.log('Database: ' + _userdb);
    }
  }

  // ==================================================
  //  Check the session status.
  // ==================================================

  function checkSession () {
    console.log('Checking session...');

    axios.get(host, config).then((response) => {
      let email = '';
      let userdb = '';
      let isValidSub = false;
      const data = response.data;

      if (data.ok === true) {
        if (data.userCtx && data.userCtx.name) {
          email = data.userCtx.name;
          for (let i = 0; i < data.userCtx.roles.length; i++) {
            if (data.userCtx.roles[i].indexOf('userdb-') === 0) {
              userdb = data.userCtx.roles[i];
            }
            if (data.userCtx.roles[i].indexOf('exp-') === 0) {
              isValidSub = moment().unix() < +data.userCtx.roles[i].slice(4);
            }
          }
          updateAuth(true, email, userdb, isValidSub, isValidSub ? 'Session authenticated. Subscription valid.' : 'Subscription expired.');
        } else {
          updateAuth(false, null, '', false, 'Session not authenticated.');
        }
      } else {
        updateAuth(false, null, '', false, 'Session not authenticated.');
      }
    }, (err) => {
      updateAuth(false, null, '', false, 'Session authentication error.');
      console.log(err);
    });
  }

  // ==================================================
  //  Login.
  // ==================================================

  function login (email, password) {
    $loginStatus.next(true);
    console.log('Logging in...');

    const credentials = {
      name: email.toLowerCase(),
      password: password
    };

    return axios.post(host, credentials, config).then((response) => {
      let userdb = '';
      let isValidSub = false;
      const data = response.data;

      if (data.ok === true) {
        if (data.name && data.roles) {
          email = data.name;
          for (let i = 0; i < data.roles.length; i++) {
            if (data.roles[i].indexOf('userdb-') === 0) {
              userdb = data.roles[i];
            }
            if (data.roles[i].indexOf('exp-') === 0) {
              isValidSub = moment().unix() < data.roles[i].slice(4);
            }
          }
          $loginStatus.next(false);
          updateAuth(true, credentials.name, userdb, isValidSub, 'Login successful.');
          $loginSuccess.next(true);
          return true;
        } else {
          $loginStatus.next(false);
          updateAuth(false, null, '', false, 'Login failed.');
          return false;
        }
      } else {
        $loginStatus.next(false);
        updateAuth(false, null, '', false, 'Login failed.');
        return false;
      }
    }, (err) => {
      $loginStatus.next(false);
      updateAuth(false, null, '', false, 'Login error.');
      console.log(err);
      return null;
    });
  }

  // ==================================================
  //  Logout.
  // ==================================================

  function logout () {
    console.log('Logging out...');

    return axios.delete(host, config).then((response) => {
      updateAuth(false, null, '', false, 'Logout successful.');
    }, (err) => {
      updateAuth(false, null, '', false, 'Logout failed.');
      console.log(err);
    });
  }

  // ==================================================
  //  Return all the stuff.
  // ==================================================

  return {
    checkSession,
    login,
    logout,
    $loginStatus,
    $loginSuccess,
    $authChanged,
    $offlineChanged,
    $isValidSub,
    get authenticated () {
      return _authenticated;
    },
    get email () {
      return _email;
    },
    get userdb () {
      return _userdb;
    },
    get isValidSub () {
      return _isValidSub;
    }
  };
}
