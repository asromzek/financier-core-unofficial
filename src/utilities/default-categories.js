/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Default Categories                                                 ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Default category definitions.                                   ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// export function DefaultCategories () {
//   return [
//     {
//       name: 'MONTHLY_BILLS',
//       categories: [{
//         name: 'RENT_MORTGAGE'
//       }, {
//         name: 'PHONE'
//       }, {
//         name: 'INTERNET'
//       }, {
//         name: 'CABLE_TV'
//       }, {
//         name: 'ELECTRICITY'
//       }, {
//         name: 'WATER'
//       }]
//     }, {
//       name: 'EVERYDAY_EXPENSES',
//       categories: [{
//         name: 'SPENDING_MONEY'
//       }, {
//         name: 'GROCERIES'
//       }, {
//         name: 'FUEL'
//       }, {
//         name: 'RESTAURANTS'
//       }, {
//         name: 'MEDICAL_DENTAL'
//       }, {
//         name: 'CLOTHING'
//       }, {
//         name: 'HOUSEHOLD_GOODS'
//       }]
//     }, {
//       name: 'RAINY_DAY_FUNDS',
//       categories: [{
//         name: 'EMERGENCY_FUND'
//       }, {
//         name: 'CAR_MAINTENANCE'
//       }, {
//         name: 'CAR_INSURANCE'
//       }, {
//         name: 'BIRTHDAYS'
//       }, {
//         name: 'CHRISTMAS'
//       }, {
//         name: 'RENTERS_INSURANCE'
//       }, {
//         name: 'RETIREMENT'
//       }]
//     }, {
//       name: 'SAVINGS_GOALS',
//       categories: [{
//         name: 'CAR_REPLACEMENT'
//       }, {
//         name: 'VACATION'
//       }]
//     }, {
//       name: 'DEBT',
//       categories: [{
//         name: 'CAR_PAYMENT'
//       }, {
//         name: 'STUDENT_LOAN_PAYMENT'
//       }]
//     }, {
//       name: 'GIVING',
//       categories: [{
//         name: 'TITHING'
//       }, {
//         name: 'CHARITABLE'
//       }]
//     }
//   ];
// }

export function DefaultCategories () {
  return [
    {
      name: 'Monthly Bills',
      categories: [{
        name: 'Rent/Mortgage'
      }, {
        name: 'Phone'
      }, {
        name: 'Internet'
      }, {
        name: 'Cable TV'
      }, {
        name: 'Electricity'
      }, {
        name: 'Water'
      }]
    }, {
      name: 'Everyday Expenses',
      categories: [{
        name: 'Spending Money'
      }, {
        name: 'Groceries'
      }, {
        name: 'Fuel'
      }, {
        name: 'Restaurants'
      }, {
        name: 'Medical/Dental'
      }, {
        name: 'Clothing'
      }, {
        name: 'Household Goods'
      }]
    }, {
      name: 'Rainy Day Funds',
      categories: [{
        name: 'Emergency Fund'
      }, {
        name: 'Car Maintenance'
      }, {
        name: 'Car Insurance'
      }, {
        name: 'Birthdays'
      }, {
        name: 'Christmas'
      }, {
        name: 'Renters Insurance'
      }, {
        name: 'Retirement'
      }]
    }, {
      name: 'Savings Goals',
      categories: [{
        name: 'Car Replacement'
      }, {
        name: 'Vacation'
      }]
    }, {
      name: 'Debt',
      categories: [{
        name: 'Car payment'
      }, {
        name: 'Student Loan Payment'
      }]
    }, {
      name: 'Giving',
      categories: [{
        name: 'Tithing'
      }, {
        name: 'Charitable'
      }]
    }
  ];
}
