/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Account Data Object                                                ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Account data object.                                  ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import uuid from 'uuid';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function Account (budgetId) {
  //
  // ##################################################
  //  Account class definition.
  // ##################################################

  class Account {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        type: null,
        closed: false,
        name: null,
        note: null,
        sort: 0,
        onBudget: true,
        checkNumber: false
      }, data);

      // --------------------------------------------------
      //  Add _id if it doesn't exist.
      // --------------------------------------------------

      if (!myData._id) {
        myData._id = Account.prefix + uuid.v4();
      }

      // --------------------------------------------------
      //  Parse the Account object ID.
      // --------------------------------------------------

      this.id = myData._id.slice(myData._id.lastIndexOf('_') + 1);

      // --------------------------------------------------
      //  Initialize the local data and trans objects.
      // --------------------------------------------------

      this.data = myData;
      this.transactions = [];

      // --------------------------------------------------
      //  Initialize the balance cache.
      // --------------------------------------------------

      this.cache = {
        clearedBalance: 0,
        unclearedBalance: 0,
        get balance () {
          return this.clearedBalance + this.unclearedBalance;
        }
      };

      // --------------------------------------------------
      //  Define value changed functions.
      // --------------------------------------------------

      this._clearedValueChangeFn = val => {
        this._changeClearedBalance(val);
      };

      this._unclearedValueChangeFn = val => {
        this._changeUnclearedBalance(val);
      };

      this._transferClearedValueChangeFn = val => {
        this._changeClearedBalance(-val);
      };

      this._transferUnclearedValueChangeFn = val => {
        this._changeUnclearedBalance(-val);
      };
    }

    // ==================================================
    //  Used when creating a transaction to  indicate
    //  that payee is an account for a transfer.
    // ==================================================

    get constructorName () {
      return 'Account';
    }

    // ==================================================
    //  Name of the account.
    // ==================================================

    get name () {
      return this.data.name;
    }

    set name (n) {
      this.data.name = n;
      this.emitChange();
    }

    // ==================================================
    //  The current balance of the account.
    // ==================================================

    get balance () {
      return this.cache.balance;
    }

    // ==================================================
    //  Account note.
    // ==================================================

    get note () {
      return this.data.note;
    }

    set note (n) {
      this.data.note = n;
      this.emitChange();
    }

    // ==================================================
    //  Specifies whether to display the check
    //  number column.
    // ==================================================

    get checkNumber () {
      return this.data.checkNumber;
    }

    set checkNumber (n) {
      this.data.checkNumber = n;
      this.emitChange();
    }

    // ==================================================
    //  Specifies if the account is on/off budget.
    // ==================================================

    get onBudget () {
      return this.data.onBudget;
    }

    set onBudget (n) {
      this.data.onBudget = n;
      this.emitChange();
    }

    // ==================================================
    //  The type of account. One of the following:
    //    * DEBIT
    //    * SAVINGS
    //    * CREDIT (credit type)
    //    * CASH
    //    * MERCHANT
    //    * MORTGAGE (credit type)
    //    * INVESTMENT
    //    * LOAN (credit type)
    // ==================================================

    get type () {
      return this.data.type;
    }

    set type (type) {
      this.data.type = type;
      this.emitChange();
    }

    // ==================================================
    //  Account list sort index.
    // ==================================================

    get sort () {
      return this.data.sort;
    }

    set sort (s) {
      if (this.data.sort !== s) {
        this.data.sort = s;
        this.emitChange();
      }
    }

    // ==================================================
    //  Returns boolean true if the account is a credit
    //  type.
    // ==================================================

    isCredit () {
      return this.data.type === 'CREDIT' ||
        this.data.type === 'OTHERCREDIT' ||
        this.data.type === 'MORTGAGE' ||
        this.data.type === 'LOAN';
    }

    // ==================================================
    //  Account open/closed status.
    // ==================================================

    get closed () {
      return this.data.closed;
    }

    set closed (c) {
      this.data.closed = c;
      this.emitChange();
    }

    // ==================================================
    //  Add a transaction to the account.
    // ==================================================

    addTransaction (trans) {
      if (trans.constructorName === 'SplitTransaction') {
        return;
      }

      this.transactions.push(trans);

      if (trans.cleared) {
        this._changeClearedBalance(trans.value);
      } else {
        this._changeUnclearedBalance(trans.value);
      }

      trans.subscribeClearedValueChange(this._clearedValueChangeFn);
      trans.subscribeUnclearedValueChange(this._unclearedValueChangeFn);
    }

    // ==================================================
    //  Remove a transaction from the account.
    // ==================================================

    removeTransaction (trans) {
      if (trans.constructorName === 'SplitTransaction') {
        return;
      }

      const index = this.transactions.indexOf(trans);
      if (index !== -1) {
        this.transactions.splice(index, 1);
      }

      if (trans.cleared) {
        this._changeClearedBalance(-trans.value);
      } else {
        this._changeUnclearedBalance(-trans.value);
      }

      trans.unsubscribeClearedValueChange(this._clearedValueChangeFn);
      trans.unsubscribeUnclearedValueChange(this._unclearedValueChangeFn);
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this.data._id;
    }

    // ==================================================
    //  Change subscription function.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    emitChange () {
      return this.fn && this.fn(this);
    }

    _changeClearedBalance (val) {
      this.cache.clearedBalance += val;
    }

    _changeUnclearedBalance (val) {
      this.cache.unclearedBalance += val;
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this.data._deleted = true;
      return this.emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this.data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_account_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Account class.
  // ==================================================

  return Account;
}
