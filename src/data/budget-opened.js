/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier BudgetOpened Data Object                                           ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier BudgetOpened data object.                             ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Class wrapper function.
// ##################################################

export function BudgetOpened () {
  //
  // ##################################################
  //  BudgetOpened class definition.
  // ##################################################

  class BudgetOpened {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize the data object.
      // --------------------------------------------------

      this._data = data;

      // --------------------------------------------------
      //  Parse the BudgetOpened object ID.
      // --------------------------------------------------

      this.id = data._id.slice(data._id.lastIndexOf('_') + 1);
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (d) {
      this._data.opened = d.opened;
      this._opened = new Date(this._data.opened);
    }

    // ==================================================
    //  Get the date object for the last time the budget
    //  was opened.
    // ==================================================

    get opened () {
      if (!this._opened && this._data.opened) {
        this._opened = new Date(this._data.opened);
      }
      return this._opened;
    }

    // ==================================================
    //  Set the last opened date to now in the database
    //  for the specified budget.
    // ==================================================

    open () {
      this._data.opened = new Date().toISOString();
      this.emitChange();
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this._data._id;
    }

    // ==================================================
    //  PouchDB object revision.
    // ==================================================

    set _rev (r) {
      this._data._rev = r;
    }

    // ==================================================
    //  Change subscription function.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    // ==================================================
    //  Change event function.
    // ==================================================

    emitChange () {
      return this.fn && this.fn(this);
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this._data;
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this._data._deleted = true;
      return this.emitChange();
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `budget-opened_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the BudgetOpened class.
  // ==================================================

  return BudgetOpened;
}
