/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Budget Manager                                                     ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier budget manager.                                       ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Project data imports.
// ##################################################

import { Month as month } from '../data/month';
import { Account as account } from '../data/account';
import { Category as category } from '../data/category';
import { Transaction as transaction } from '../data/transaction';
import { MasterCategory as masterCategory } from '../data/master-category';
import { MonthCategory as monthCategory } from '../data/month-category';
import { Payee as payee } from '../data/payee';

// ##################################################
//  Project manager imports.
// ##################################################

import { MonthManager as monthManager } from '../managers/month-manager';

// ##################################################
//  Project utility imports.
// ##################################################

import { DefaultCategories as defaultCategories } from '../utilities/default-categories';

// ##################################################
//  BudgetManager function definition.
// ##################################################

export function BudgetManager (pouch, budgetId) {
  //
  // ==================================================
  //  Data class declarations with global budgetId set.
  // ==================================================

  const Month = month(budgetId);
  const Account = account(budgetId);
  const Category = category(budgetId);
  const Transaction = transaction(budgetId);
  const MasterCategory = masterCategory(budgetId);
  const MonthManager = monthManager(budgetId);
  const MonthCategory = monthCategory(budgetId);
  const Payee = payee(budgetId);
  const DefaultCategories = defaultCategories();

  // ==================================================
  //  Public functions and variables.
  // ==================================================

  const ret = {
    accounts: accounts(),
    categories: categories(),
    masterCategories: masterCategories(),
    payees: payees(),
    budget: budget(),
    remove,
    initialize: initializeAllCategories,
    put
  };

  // ==================================================
  //  Return available functions and variables.
  // ==================================================

  return ret;

  // ==================================================
  //  Mark PouchDB object as deleted.
  // ==================================================

  function remove () {
    return pouch.allDocs({
      startkey: `b_${budgetId}_`,
      endkey: `b_${budgetId}_\uffff`,
      include_docs: true
    }).then(res => {
      return pouch.bulkDocs(res.rows.map(row => {
        return {
          _id: row.doc._id,
          _rev: row.doc._rev,
          _deleted: true
        };
      }));
    });
  }

  // ==================================================
  //  Insert or update PouchDB object.
  // ==================================================

  function put (o) {
    return pouch.put(o.toJSON()).then(res => {
      o.data._rev = res.rev;
    });
  }

  // ==================================================
  //  Account functions.
  // ==================================================

  function accounts () {
    //
    // --------------------------------------------------
    //  Get all accounts from the database.
    // --------------------------------------------------

    function all () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Account.startKey,
        endkey: Account.endKey
      }).then(res => {
        const accounts = [];

        for (let i = 0; i < res.rows.length; i++) {
          const acc = new Account(res.rows[i].doc);
          accounts.push(acc);
        }

        return accounts;
      });
    }

    // --------------------------------------------------
    //  Get account by ID from the database.
    // --------------------------------------------------

    function get (accountId) {
      return pouch.get(Account.prefix + accountId).then(acc => {
        return new Account(acc);
      });
    }

    // --------------------------------------------------
    //  Return available functions.
    // --------------------------------------------------

    return {
      all,
      get
    };
  }

  // ==================================================
  //  Master category functions.
  // ==================================================

  function masterCategories () {
    //
    // --------------------------------------------------
    //  Get all master categories from the database.
    // --------------------------------------------------

    function all () {
      return pouch.allDocs({
        include_docs: true,
        startkey: MasterCategory.startKey,
        endkey: MasterCategory.endKey
      }).then(res => {
        const ret = {};

        for (let i = 0; i < res.rows.length; i++) {
          const cat = new MasterCategory(res.rows[i].doc);
          cat.subscribe(put);
          ret[cat.id] = cat;
        }

        return ret;
      });
    }

    // --------------------------------------------------
    //  Return available functions.
    // --------------------------------------------------

    return {
      all,
      put
    };
  }

  // ==================================================
  //  Category functions.
  // ==================================================

  function categories () {
    //
    // --------------------------------------------------
    //  Get all categories from the database.
    // --------------------------------------------------

    function all () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Category.startKey,
        endkey: Category.endKey
      }).then(res => {
        const ret = {};

        for (let i = 0; i < res.rows.length; i++) {
          const cat = new Category(res.rows[i].doc);
          cat.subscribe(put);
          ret[cat.id] = cat;
        }

        return ret;
      });
    }

    // --------------------------------------------------
    //  Return available functions.
    // --------------------------------------------------

    return {
      all,
      put
    };
  }

  // ==================================================
  //  Payee functions.
  // ==================================================

  function payees () {
    //
    // --------------------------------------------------
    //  Get all payees from the database.
    // --------------------------------------------------

    function all () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Payee.startKey,
        endkey: Payee.endKey
      }).then(res => {
        const ret = {};

        for (let i = 0; i < res.rows.length; i++) {
          const myPayee = new Payee(res.rows[i].doc);
          myPayee.subscribe(put);
          ret[myPayee.id] = myPayee;
        }

        return ret;
      });
    }

    // --------------------------------------------------
    //  Return available functions.
    // --------------------------------------------------

    return {
      all
    };
  }

  // ==================================================
  //  Add default categories to new budget.
  // ==================================================

  function initializeAllCategories () {
    const promises = [];

    for (let i = 0; i < DefaultCategories.length; i++) {
      const masterCat = new MasterCategory({
        name: DefaultCategories[i].name,
        sort: i
      });

      promises.push(pouch.put(masterCat.toJSON()));

      for (let j = 0; j < DefaultCategories[i].categories.length; j++) {
        const cat = new Category({
          name: DefaultCategories[i].categories[j].name, // TODO: Restore transations.
          sort: j,
          masterCategory: masterCat.id
        });

        promises.push(pouch.put(cat.toJSON()));
      }
    }

    return Promise.all(promises);
  }

  // ==================================================
  //  Budget functions.
  // ==================================================

  function budget () {
    //
    // --------------------------------------------------
    //  Get all month-categories from the database.
    // --------------------------------------------------

    function getAllMonthCategories () {
      return pouch.allDocs({
        include_docs: true,
        startkey: MonthCategory.startKey(budgetId),
        endkey: MonthCategory.endKey(budgetId)
      }).then(res => {
        return res.rows.map(row => {
          const bValue = new MonthCategory(row.doc);
          bValue.subscribe(put);

          return bValue;
        });
      });
    }

    // --------------------------------------------------
    //  Get all accounts from the database.
    // --------------------------------------------------

    function getAllAccounts () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Account.startKey,
        endkey: Account.endKey
      }).then(res => {
        return res.rows.map(row => {
          const acc = new Account(row.doc);
          acc.subscribe(put);

          return acc;
        });
      });
    }

    // --------------------------------------------------
    //  Get all transactions from the database.
    // --------------------------------------------------

    function getAllTransactions () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Transaction.startKey,
        endkey: Transaction.endKey
      }).then(res => {
        const transactions = {};

        for (let i = 0; i < res.rows.length; i++) {
          const trans = new Transaction(res.rows[i].doc);

          // Add transaction splits
          if (trans.splits.length) {
            for (let j = 0; j < trans.splits.length; j++) {
              transactions[trans.splits[j].id] = trans.splits[j];
            }
          }

          transactions[trans.id] = trans;
        }

        return Object.keys(transactions).map(key => {
          const trans = transactions[key];

          if (trans.data.transfer) {
            trans.transfer = transactions[trans.data.transfer];
          }

          return transactions[key];
        });
      });
    }

    // --------------------------------------------------
    //  Get all budget objects from the database.
    // --------------------------------------------------

    function all () {
      return pouch.allDocs({
        include_docs: true,
        startkey: Month.startKey,
        endkey: Month.endKey
      }).then(res => {
        const months = res.rows.map(row => new Month(row.doc, put));
        const manager = new MonthManager(months, put);

        return getAllMonthCategories().then(monthCatVals => {
          for (let i = 0; i < monthCatVals.length; i++) {
            manager.addMonthCategory(monthCatVals[i]);
          }

          return getAllAccounts().then(accounts => {
            for (let i = 0; i < accounts.length; i++) {
              manager.addAccount(accounts[i]);
            }

            return getAllTransactions().then(transactions => {
              for (let i = 0; i < transactions.length; i++) {
                manager.addTransaction(transactions[i]);
              }

              return manager;
            });
          });
        });
      });
    }

    // --------------------------------------------------
    //  Return available functions.
    // --------------------------------------------------

    return all;
  }
}
