/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Month Manager                                                      ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier month manager.                                        ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Project data imports.
// ##################################################

import { Month as month } from '../data/month';
import { Account as account } from '../data/account';

// ##################################################
//  MonthManager function definition.
// ##################################################

export function MonthManager (budgetId) {
  //
  // ==================================================
  //  Initialize data object classes.
  // ==================================================

  const Month = month(budgetId);
  const Account = account(budgetId);

  // ##################################################
  //  MonthManager class definition.
  // ##################################################

  class MonthManager {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (months = [], saveFn) {
      if (months.length === 0) {
        months.push(new Month(Month.createID(new Date()), saveFn));
      }

      months = this._fillMonthGaps(months);

      for (let i = 0; i < months.length; i++) {
        if (months[i + 1]) {
          this._linkMonths(months[i], months[i + 1]);
        }
      }

      this.months = months;
      this.accounts = [];
      this.allAccounts = new Account({
        name: 'All Accounts'
      });
      this.transactions = {};
      this.saveFn = saveFn;
    }

    // ==================================================
    // Add transaction to the relevent month.
    // ==================================================

    addTransaction (trans) {
      const myMonth = this.getMonth(trans.month);
      const myAccount = this.getAccount(trans.account);

      trans.subscribe(this.saveFn);

      trans.removeTransaction = t => this.removeTransaction(t);
      trans.addTransaction = t => this.addTransaction(t);

      trans.subscribeAccountChange((newAccountId, oldAccountId) => {
        const oldAccount = this.getAccount(oldAccountId);
        const currentMonth = this.getMonth(trans.month);

        if (oldAccount) {
          oldAccount.removeTransaction(trans);
        }

        const newAccount = this.getAccount(newAccountId);
        if (newAccount) {
          newAccount.addTransaction(trans);
        }

        if ((!oldAccount || !oldAccount.onBudget) && newAccount && newAccount.onBudget) {
          currentMonth.addTransaction(trans);
        } else if ((oldAccount && oldAccount.onBudget) && (!newAccount || !newAccount.onBudget)) {
          currentMonth.removeTransaction(trans);
        }
      });

      this.transactions[trans.id] = trans;

      trans.subscribeCategoryChange(() => {
        // Before change.
        const month = this.getMonth(trans.month);
        const account = this.getAccount(trans.account);
        if (account && account.onBudget) {
          month.removeTransaction(trans);
          month.startRolling(trans.category);
        }
      }, () => {
        // After change.
        const month = this.getMonth(trans.month);
        const account = this.getAccount(trans.account);
        if (account && account.onBudget) {
          month.addTransaction(trans);
        }
      });

      trans.subscribeMonthChange((newMonth, oldMonth) => {
        const m = this.getMonth(newMonth);
        const account = this.getAccount(trans.account);

        if (account && account.onBudget) {
          this.getMonth(oldMonth).removeTransaction(trans);
          m.addTransaction(trans);
        }
      });

      if (myAccount && myAccount.onBudget) {
        myMonth.addTransaction(trans);
      }

      if (myAccount) {
        myAccount.addTransaction(trans);
      }

      this.allAccounts.addTransaction(trans);
    }

    // ==================================================
    //  Remove transaction from the month.
    // ==================================================

    removeTransaction (trans) {
      const transactions = [trans];

      if (trans.transfer) {
        transactions.push(trans.transfer);
      }

      transactions.forEach(transaction => {
        if (this.transactions[transaction.id]) {
          if (transaction.splits && transaction.splits.length) {
            transaction.splits.forEach(split => {
              this.removeTransaction(split);
            });
          }

          delete this.transactions[transaction.id];

          const myMonth = this.getMonth(transaction.month);
          const myAccount = this.getAccount(transaction.account);

          if (myAccount) {
            myAccount.removeTransaction(transaction);
          }

          this.allAccounts.removeTransaction(transaction);

          if (myAccount && myAccount.onBudget) {
            myMonth.removeTransaction(transaction);
            myMonth.startRolling(transaction.category);
          }

          transaction.subscribeAccountChange(null);
          transaction.subscribeCategoryChange(null, null);
          transaction.subscribeMonthChange(null);
        }
      });
    }

    // ==================================================
    //  Add a MonthCategory to the month.
    // ==================================================

    addMonthCategory (monthCat) {
      this.getMonth(MonthManager._dateIDToDate(monthCat.monthId)).addBudget(monthCat);
    }

    // ==================================================
    //  This function is used to fill gaps of in months.
    //  We can already assume the months are in order, as
    //  promised by CouchDB/PouchDB.
    //
    //  *NOTE:* Month objects must be ordered oldest to
    //          newest!
    //
    //  *NOTE:* Does not link months!
    //
    //  Example: [Feb, Apr, May, Aug] =>
    //             [Feb, Mar, Apr, May, Jun, Jul, Aug]
    // ==================================================

    _fillMonthGaps (months) {
      if (months.length <= 1) {
        return months;
      }

      let endIndex = months.length - 1;
      for (var i = 0; i < endIndex; i++) {
        const startMonth = MonthManager._dateIDToDate(months[i].date);
        const endMonth = MonthManager._dateIDToDate(months[i + 1].date);
        const diff = MonthManager._diff(startMonth, endMonth);

        if (diff > 1) {
          const monthsToAdd = diff - 1;
          const newMonths = [];

          let id = MonthManager._nextDateID(months[i].date);

          for (let j = 0; j < monthsToAdd; j++) {
            const m = new Month(id, this.saveFn);
            newMonths.push(m);

            id = MonthManager._nextDateID(id);
          }
          months.splice.apply(months, [i + 1, 0].concat(newMonths));

          i += monthsToAdd;
          endIndex += monthsToAdd;
        } else if (diff < 1) {
          throw new Error('Provided months are out of order or duplicates exist!');
        }
      }

      return months;
    }

    // ==================================================
    //  Get any Month from a given date.
    // ==================================================

    getMonth (date) {
      const diff = MonthManager._diff(MonthManager._dateIDToDate(this.months[0].date), date);

      if ((diff + 1) > this.months.length) {
        // We need to append months.
        const monthsToAdd = (diff + 1) - this.months.length;
        let d = this.months[this.months.length - 1].date;

        for (let i = 0; i < monthsToAdd; i++) {
          d = MonthManager._nextDateID(d);
          const m = new Month(d, this.saveFn);
          this.months.push(m);
          this._linkMonths(this.months[this.months.length - 2], this.months[this.months.length - 1]);
        }

        // This is the only case when we need to propagate rolling
        // prepended months will by definition not have any data rolling
        // into them, so we don't need to do this there
        this._propagateRollingFromMonth(this.months[(this.months.length - 1) - monthsToAdd]);

        return this.months[this.months.length - 1];
      }

      if (diff < 0) {
        // we need to prepend months.
        const monthsToAdd = Math.abs(diff);
        let d = this.months[0].date;

        for (let i = 0; i < monthsToAdd; i++) {
          d = MonthManager._previousDateID(d);
          const m = new Month(d, this.saveFn);
          this.months.unshift(m);
          this._linkMonths(this.months[0], this.months[1]);
        }
        return this.months[0];
      }

      // We already have the month.
      return this.months[diff];
    }

    // ==================================================
    //  Get account by ID.
    // ==================================================

    getAccount (id) {
      for (let i = 0; i < this.accounts.length; i++) {
        if (this.accounts[i].id === id) {
          return this.accounts[i];
        }
      }
    }

    // ==================================================
    //  Add an account.
    // ==================================================

    addAccount (acc) {
      acc.subscribe(this.saveFn);
      this.accounts.push(acc);
    }

    // ==================================================
    //  Remove an account.
    // ==================================================

    removeAccount (acc) {
      const index = this.accounts.indexOf(acc);

      for (let i = 0; i < acc.transactions.length; i++) {
        if (acc.onBudget) {
          const month = this.getMonth(acc.transactions[i].month);
          month.removeTransaction(acc.transactions[i]);
        }

        acc.removeTransaction(acc.transactions[i]);
      }

      if (index !== -1) {
        this.accounts.splice(index, 1);
      } else {
        throw new Error(`Account with id ${acc.id} not found!`);
      }
    }

    // ==================================================
    //  Link one month to the next.
    // ==================================================

    _linkMonths (firstMonth, secondMonth) {
      firstMonth.subscribeNextMonth(secondMonth);
    }

    // ==================================================
    //  Upon initialization and after all Month objects,
    //  transactions, etc. have been added, call this to
    //  calculate any rolling balances, etc.
    // ==================================================

    propagateRolling (categoryIds) {
      this.categoryIds = categoryIds;

      for (var i = 0; i < categoryIds.length; i++) {
        this.months[0].startRolling(categoryIds[i]);
      }
    }

    // ==================================================
    //  If months are added in the future, we must
    //  propagate balances, etc to those month(s).
    //
    //  This will not do anything if `propagateRolling()`
    //  has not already been called (since we'll probably
    //  call `propagateRolling()` in the future).
    // ==================================================

    _propagateRollingFromMonth (month) {
      //
      // --------------------------------------------------
      // If categoryIds is not set,
      // we're probably going to call this.propagateRolling
      // in the future, so doing this is moot...
      // --------------------------------------------------

      if (this.categoryIds) {
        for (var i = 0; i < this.categoryIds.length; i++) {
          month.startRolling(this.categoryIds[i]);
        }
      }
    }

    // ==================================================
    //  Check the difference, in months, between two
    //  Month objects.
    // ==================================================

    static _diff (d1, d2) {
      let months;

      months = (d2.getFullYear() - d1.getFullYear()) * 12;
      months -= d1.getMonth();
      months += d2.getMonth();

      return months;
    }

    // ==================================================
    //  Convert a Month dateID to a date object.
    // ==================================================

    static _dateIDToDate (date) {
      const [year, month] = date.split('-');
      return new Date(year, month - 1, 1);
    }

    // ==================================================
    //  Get the next date ID from a date ID.
    // ==================================================

    static _nextDateID (date) {
      const [year, month] = date.split('-');
      return Month.createID(new Date(year, month, 1));
    }

    // ==================================================
    //  Get the previous date ID from a date ID.
    // ==================================================

    static _previousDateID (date) {
      const [year, month] = date.split('-');
      return Month.createID(new Date(year, month - 2, 1));
    }
  }

  // ==================================================
  //  Return the MonthManager class.
  // ==================================================

  return MonthManager;
}
